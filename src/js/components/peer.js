import React from 'react';
import Avatar from 'avataaars';

const defaultAvatar = {
  avatarStyle: 'Circle',
  topType: 'WinterHat4',
  accessoriesType: 'Blank',
  hatColor: 'Red',
  facialHairType: 'Blank',
  clotheType: 'BlazerShirt',
  eyeType: 'Default',
  eyebrowType: 'Default',
  mouthType: 'Default',
  skinColor: 'Light',
};

export default ({peer}) => {
  const avatar = peer.getIn(['profile', 'avatar']);
  const profile = peer.get('profile');

  return (
    <div className="peer">
      <Avatar
        style={{width: '50px', height: '50px'}}
        {...defaultAvatar}
        {...(avatar ? avatar.toJS() : {})}
      />
      <div className="peer--name">{peer.getIn(['profile', 'name'])}</div>
    </div>
  );
};
