import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

export const modalify = (Component) => {
  return ({open, ...props}) => {
    if (!open) return null;
    return ReactDOM.createPortal(
      (
        <Fragment>
          <div className="modal--screen" />
          <div className="modal--container">
            <Component {...props} />
          </div>
        </Fragment>
      ),
      document.getElementById('modal'),
    );
  };
};

export const ModalHeader = ({children}) => {
  return (
    <div className="modal--header">{children}</div>
  );
};

export const ModalContent = ({children}) => {
  return (
    <div className="modal--content">
      {children}
    </div>
  );
};
