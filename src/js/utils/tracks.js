export const formatTrack = ({track}) => {
  let text = '';

  if (track.get('artist')) {
    text += `${track.get('artist')} -`;
  }
  if (track.get('name')) {
    text += track.get('name');
  } else {
    text += track.get('filename');
  }

  return text;
};
