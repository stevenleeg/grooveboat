export default [
  {
    key: 'topType',
    label: 'Top',
    variants: [
      {label: 'No hair', key: 'NoHair'},
      {label: 'Turban', key: 'Turban'},
      {label: 'Hat', key: 'Hat'},
      {label: 'Big fro', key: 'LongHairFro'},
      {label: 'Straight long hair', key: 'LongHairStraight'},
    ],
  },
  {
    key: 'facialHairType',
    label: 'Facial Hair',
    variants: [
      {label: 'Medium beard', key: 'BeardMedium'},
      {label: 'Light beard', key: 'BeardLight'},
      {label: 'Magestic beard', key: 'BeardMagestic'},
    ],
  },
  {
    key: 'clotheType',
    label: 'Clothes',
    variants: [
      {label: 'Blazer shirt', key: 'BlazerShirt'},
      {label: 'Blazer sweater', key: 'BlazerSweater'},
      {label: 'Shirt', key: 'GraphicShirt'},
      {label: 'Hoodie', key: 'Hoodie'},
    ],
  },
  {
    key: 'skinColor',
    label: 'Skin color',
    variants: [
      {label: 'Tanned', key: 'Tanned'},
      {label: 'Yellow', key: 'Yellow'},
      {label: 'Pale', key: 'Pale'},
      {label: 'Black', key: 'Black'},
      {label: 'Light', key: 'Light'},
      {label: 'Brown', key: 'Brown'},
      {label: 'DarkBrown', key: 'DarkBrown'},
    ],
  },
  {
    key: 'eyeType',
    label: 'Eye shape',
    variants: [
      {label: 'Circles', key: 'Default'},
      {label: 'Wink', key: 'Wink'},
      {label: 'Surprised', key: 'Surprised'},
      {label: 'Sideways Glance', key: 'Side'},
    ],
  },
];
