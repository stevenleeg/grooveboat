import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faCog, faUser} from '@fortawesome/free-solid-svg-icons';

import store from './store';
import Router from './router';
import '../scss/app.scss';

library.add(faCog, faUser);

const App = () => (
  <Provider store={store}>
    <Router />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
