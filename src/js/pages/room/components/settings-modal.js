import React, {Fragment} from 'react';
import {kea} from 'kea';
import {modalify, ModalContent, ModalHeader} from 'utils/modal';
import AvatarOptions from 'utils/avatar-options';
import Avatar from 'avataaars';

import ProfileService from 'services/profile';

@modalify
@kea({
  connect: {
    actions: [
      ProfileService, ['setVariant', 'setName'],
    ],

    props: [
      ProfileService, ['profile'],
    ],
  },
})
export default class SettingsModal extends React.Component {
  changeVariant = ({key}) => {
    return e => this.actions.setVariant({key, value: e.target.value});
  }

  render() {
    const {profile, closeModal} = this.props;

    if (!profile) return null;

    return (
      <Fragment>
        <ModalHeader>Settings</ModalHeader>
        <ModalContent>
          <div className="settings--avatar">
            <Avatar
              style={{width: '100px', height: '100px'}}
              {...profile.get('avatar').toJS()}
            />
            <div className="settings--name">{profile.get('name')}</div>
          </div>
          <label>screen name:</label>
          <input
            type="text"
            placeholder="music_lover8"
            value={profile.get('name')}
            onChange={(e) => this.actions.setName({name: e.target.value})}
          />
          <div className="settings--avatar-config">
            {AvatarOptions.map((option) => {
              return (
                <Fragment key={option.key}>
                  <label>{option.label}</label>
                  <select
                    onChange={this.changeVariant({key: option.key})}
                    value={profile.getIn(['avatar', option.key])}
                  >
                    {option.variants.map((variant) => {
                      return (
                        <option
                          key={variant.key}
                          value={variant.key}
                        >
                          {variant.label}
                        </option>
                      );
                    })}
                  </select>
                </Fragment>
              );
            })}
          </div>
          <button onClick={closeModal}>done</button>
        </ModalContent>
      </Fragment>
    );
  }
}
