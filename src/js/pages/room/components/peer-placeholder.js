import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export default ({onClick}) => (
  <div className="placeholder" onClick={onClick}>
    <FontAwesomeIcon icon="user" />
    <div className="name">join</div>
  </div>
);
