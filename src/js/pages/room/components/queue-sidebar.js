import React from 'react';
import Dropzone from 'react-dropzone';
import {kea} from 'kea';

import {formatTrack} from 'utils/tracks';
import LibraryService from 'services/library';

@kea({
  path: () => ['kea', 'pages', 'room', 'queue-sidebar'],

  connect: {
    actions: [
      LibraryService, ['addTrack'],
    ],

    props: [
      LibraryService, ['selectedQueueWithTracks as selectedQueue'],
    ],
  },
})
export default class QueueSidebar extends React.Component {
  onDrop = ([file]) => {
    this.actions.addTrack({file});
  }

  render() {
    const {selectedQueue} = this.props;

    if (!selectedQueue) return false;

    return (
      <div className="sidebar--queue">
        <div className="queue--name">{selectedQueue.get('name')}</div>
        <ol>
          {selectedQueue.get('tracks').map((track) => {
            return (
              <li key={track.get('_id')}>
                {formatTrack({track})}
              </li>
            );
          })}
        </ol>
        <Dropzone onDrop={this.onDrop}>
          drop a file for a good time
        </Dropzone>
      </div>
    );
  }
}
