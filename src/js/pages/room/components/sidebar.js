import React from 'react';

import QueueSidebar from './queue-sidebar';

const TAB_QUEUE = 'queue';
const TAB_CHAT = 'chat';

export default class Sidebar extends React.Component {
  state = {
    tab: TAB_QUEUE,
  }
  render() {
    const {tab} = this.state;
    return (
      <div className="room--sidebar">
        <div className="room--sidebar-tabs">
          <div className="tab active">queue</div>
          <div className="tab">chat</div>
        </div>
        {tab === TAB_QUEUE && <QueueSidebar />}
      </div>
    );
  }
}
