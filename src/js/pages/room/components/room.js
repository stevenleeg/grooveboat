import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import {kea} from 'kea';

import {formatTrack} from 'utils/tracks';
import RoomService from 'services/rooms';
import JukeboxService from 'services/jukebox';
import Peer from 'components/peer';
import PeerPlaceholder from './peer-placeholder';

@kea({
  connect: {
    actions: [
      RoomService, ['becomeDj'],
    ],

    props: [
      RoomService, ['djs', 'audience'],
      JukeboxService, ['currentTrack'],
    ],
  },
})
export default class Room extends React.PureComponent {
  render() {
    const {djs, audience, currentTrack} = this.props;

    const range = new Immutable.Range(0, 5);
    const marquee = currentTrack ? formatTrack({track: currentTrack}) : 'waiting';

    return (
      <div className="room--content">
        <div className="room--stage">
          <div className="room--djs">
            {range.map((_, i) => {
              const dj = djs.get(i, false);
              return (
                <div className="slot" key={dj ? dj.get('id') : i}>
                  {dj ? (
                    <Peer peer={dj} />
                  ) : (
                    <PeerPlaceholder onClick={this.actions.becomeDj} />
                  )}
                </div>
              );
            })}
          </div>

          <div className="room--nowplaying">
            {marquee}
          </div>
        </div>

        <div className="room--audience">
          {audience.map((peer) => <Peer key={peer.get('id')} peer={peer} />)}
        </div>
      </div>
    );
  }
}
