import React, {Fragment} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {withRouter} from 'react-router';

import Room from './components/room';
import Sidebar from './components/sidebar';
import SettingsModal from './components/settings-modal';
import Kea from './kea';

@withRouter
@Kea
export default class RoomPage extends React.Component {
  componentDidMount() {
    const {id} = this.props.match.params;
    this.actions.init({
      roomId: id,
      notFoundCallback: () => this.props.history.push('/rooms/new'),
    });
  }

  render() {
    const {id} = this.props.match.params;
    const {showSettingsModal} = this.props;

    return (
      <Fragment>
        <SettingsModal
          closeModal={this.actions.toggleSettingsModal}
          open={showSettingsModal}
        />
        <div className="room--topbar">
          <div className="left">
            grooveboat
          </div>
          <div className="spacer" />
          <div className="right" onClick={this.actions.toggleSettingsModal}>
            <FontAwesomeIcon icon="cog" />
          </div>
        </div>
        <div className="room--container">
          <Room />
          <Sidebar />
        </div>
      </Fragment>
    );
  }
}
