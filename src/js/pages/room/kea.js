import {kea} from 'kea';
import PropTypes from 'prop-types';
import {put, take, call} from 'redux-saga/effects';

import RoomService from 'services/rooms';

export default kea({
  connect: {
    actions: [
      RoomService, ['joinRoom', 'joinRoomSuccess', 'joinRoomFailure'],
    ],
  },

  actions: () => ({
    init: ({roomId, notFoundCallback}) => ({roomId, notFoundCallback}),
    toggleSettingsModal: true,
  }),

  reducers: ({actions}) => ({
    showSettingsModal: [false, PropTypes.bool, {
      [actions.toggleSettingsModal]: showSettingsModal => !showSettingsModal,
    }],
  }),

  takeEvery: ({actions}) => ({
    [actions.init]: function* ({payload: {roomId, notFoundCallback}}) {
      // Attempt to join the room
      yield put(this.actions.joinRoom({id: roomId}));
      const {type} = yield take([
        this.actions.joinRoomSuccess.toString(),
        this.actions.joinRoomFailure.toString(),
      ]);

      // If the room doesn't exist we'll need to create it
      if (type === this.actions.joinRoomFailure.toString()) {
        yield call(notFoundCallback);
      }
    },
  }),
});
