import Immutable from 'immutable';
import {take, put, call} from 'redux-saga/effects';
import PropTypes from 'prop-types';
import {kea} from 'kea';

import RoomService from 'services/rooms';

const initialInputs = Immutable.fromJS({
  name: '',
});

export default kea({
  path: () => ['kea', 'views', 'create-room'],

  connect: {
    actions: [
      RoomService, ['createRoom', 'createRoomSuccess'],
    ],
  },

  actions: () => ({
    changeInput: ({name, value}) => ({name, value}),
    submit: ({callback}) => ({callback}),
  }),

  reducers: ({actions}) => ({
    inputs: [initialInputs, PropTypes.instanceOf(Immutable.Map), {
      [actions.changeInput]: (state, {name, value}) => {
        return state.merge({[name]: value});
      },
    }],
  }),

  takeEvery: ({actions}) => ({
    [actions.submit]: function* ({payload}) {
      const inputs = yield this.get('inputs');
      yield put(actions.createRoom({name: inputs.get('name')}));
      const {payload: successPayload} = yield take(actions.createRoomSuccess.toString());
      yield call(payload.callback, {room: successPayload.room});
    },
  }),
});
