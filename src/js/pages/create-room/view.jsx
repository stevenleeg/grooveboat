import React from 'react';
import {withRouter} from 'react-router';

import RoomStore from 'services/rooms';
import layout from 'components/layout';
import Kea from './kea';

@withRouter
@layout
@Kea
export default class CreateRoomPage extends React.Component {
  submit = () => {
    const {inputs} = this.props;
    this.actions.submit({
      callback: ({room}) => this.props.history.push(`/rooms/${room.get('id')}`),
    });
  }

  render() {
    const {inputs} = this.props;
    const {changeInput} = this.actions;

    return (
      <div className="room-selector--container">
        <div className="room-selector--box">
          <h1>create a room</h1>
          <input
            placeholder="Name"
            value={inputs.get('name')}
            onChange={e => changeInput({name: 'name', value: e.target.value})}
          />
          <button onClick={this.submit}>create room</button>
        </div>
      </div>
    );
  }
}
