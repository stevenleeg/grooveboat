import React from 'react';
import {withRouter} from 'react-router';

import RoomStore from 'services/rooms';
import layout from 'components/layout';
import Kea from './kea';

@withRouter
@layout
@Kea
export default class RoomSelectorPage extends React.Component {
  render() {
    const {rooms} = this.props;
    return (
      <div className="room-selector--container">
        <div className="room-selector--box">
          <h1>select a room</h1>
          {rooms.count() === 0 && (
            <p>No rooms found</p>
          )}
          <div className="room-selector--actions">
            <button
              onClick={() => this.props.history.push('/rooms/new')}
            >
              create a room
            </button>
          </div>
        </div>
      </div>
    );
  }
}
