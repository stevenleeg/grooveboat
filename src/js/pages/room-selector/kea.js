import {kea} from 'kea';
import {put} from 'redux-saga/effects';

import RoomService from 'services/rooms';

export default kea({
  connect: {
    actions: [
      RoomService, ['fetchRooms'],
    ],

    props: [
      RoomService, ['rooms'],
    ],
  },

  path: () => ['kea', 'views', 'room-selector'],

  start: function* () {
    yield put(this.actions.fetchRooms());
  },
});
