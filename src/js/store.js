import {getStore} from 'kea';
import sagaPlugin from 'kea-saga';
import {createLogger} from 'redux-logger';

export default getStore({
  plugins: [sagaPlugin],
  middleware: [createLogger({collapsed: true})],
});
