import React from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import {connect} from 'kea';

import ConnectionService from 'services/connection';
import LibraryService from 'services/library';
import JukeboxService from 'services/jukebox';
import ProfileService from 'services/profile';

import RoomSelectorPage from './pages/room-selector/view';
import CreateRoomPage from './pages/create-room/view';
import RoomPage from './pages/room/view';

@connect({
  actions: [
    ConnectionService, ['connect'],
    LibraryService, ['init as initLibrary'],
    JukeboxService, ['init as initJukebox'],
    ProfileService, ['init as initProfile'],
  ],
})
export default class Router extends React.Component {
  componentDidMount() {
    this.actions.connect();
    this.actions.initLibrary();
    this.actions.initJukebox();
    this.actions.initProfile();
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/rooms/new" component={CreateRoomPage} />
          <Route exact path="/rooms/:id" component={RoomPage} />
          <Route exact path="/" component={RoomSelectorPage} />
        </Switch>
      </BrowserRouter>
    );
  }
}
