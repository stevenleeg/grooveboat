import Immutable from 'immutable';
import PropTypes from 'prop-types';
import {call, take, put} from 'redux-saga/effects';
import {eventChannel, END} from 'redux-saga';
import {kea} from 'kea';
import JSMediaTags from 'jsmediatags';
import uuid from 'uuid/v1';

import db from 'utils/db';

const readFile = ({file}) => {
  return eventChannel((emitter) => {
    const reader = new FileReader();

    reader.onloadend = () => {
      emitter(reader.result);
      emitter(END);
    };

    reader.readAsArrayBuffer(file);
    return () => {};
  });
};

const readTags = ({file}) => {
  return new Promise((resolve, reject) => {
    JSMediaTags.read(file, {onSuccess: resolve, onError: reject});
  });
};

export default kea({
  path: () => ['kea', 'services', 'library'],

  actions: () => ({
    init: true,

    addTrack: ({file}) => ({file}),
    addTrackSuccess: ({track}) => ({track}),
    setTrack: ({track}) => ({track}),
    requeueTopTrack: true,

    setSelectedQueue: ({queue}) => ({queue}),
    addToQueue: ({trackId, queueId}) => ({trackId, queueId}),
  }),

  reducers: ({actions}) => ({
    selectedQueue: [null, PropTypes.instanceOf(Immutable.Map), {
      [actions.setSelectedQueue]: (state, {queue}) => queue,
      [actions.requeueTopTrack]: (state) => {
        // Put the top track at the end of the queue
        const trackIds = state.get('trackIds');
        const topTrackId = trackIds.first();
        return state.merge({
          trackIds: trackIds.shift().push(topTrackId),
        });
      },
    }],

    tracks: [new Immutable.Map(), PropTypes.instanceOf(Immutable.List), {
      [actions.setTrack]: (state, {track}) => {
        return state.set(track.get('_id'), track);
      },
      [actions.addTrackSuccess]: (state, {track}) => {
        return state.set(track.get('_id'), track);
      },
    }],
  }),

  selectors: ({selectors}) => ({
    selectedQueueWithTracks: [
      () => [selectors.selectedQueue, selectors.tracks],
      (queue, tracks) => queue && queue.merge({
        tracks: queue.get('trackIds')
          .filter(id => !!tracks.get(id))
          .map(id => tracks.get(id)),
      }),
      PropTypes.instanceOf(Immutable.Map),
    ],
  }),

  takeEvery: ({actions}) => ({
    [actions.init]: function* () {
      let queues;
      try {
        queues = yield call(db.get, 'queues');
      } catch (e) {
        if (e.name !== 'not_found') {
          console.log('Something went wrong fetching the queue', queues);
          return;
        }

        // Looks like they don't have a default queue, so let's create one!
        const defaultQueue = Immutable.fromJS({
          _id: `queues/${uuid()}`,
          name: 'Default Queue',
          trackIds: [],
        });

        queues = Immutable.fromJS({
          _id: 'queues',
          defaultId: defaultQueue.get('_id'),
          queueIds: [defaultQueue.get('_id')],
        });

        yield call(db.put, defaultQueue);
        yield call(db.put, queues);
      }

      // Fetch the default queue
      const queue = yield call(db.get, queues.get('defaultId'));
      yield put(this.actions.setSelectedQueue({queue}));

      // Fetch and cache each track in the queue
      for (const trackId of queue.get('trackIds').toJS()) {
        const track = yield call(db.get, trackId);
        yield put(this.actions.setTrack({track}));
      }
    },

    [actions.addTrack]: function* ({payload: {file}}) {
      // Parse ID3 tags out of file
      const id3 = (yield call(readTags, {file})).tags;

      const filename = file.name.split('.').slice(0, -1).join('.');
      const tags = {
        filename,
        artist: (id3.TPE1 ? id3.TPE1.data.trim() : null),
        album: (id3.TALB ? id3.TALB.data.trim() : null),
        track: (id3.TIT2 ? id3.TIT2.data.trim() : null),
      };

      // Pin the track onto IPFS
      const fileChannel = yield call(readFile, {file});
      const buffer = yield take(fileChannel);
      const resp = yield call(window.ipfs.add, Buffer.from(buffer), {pin: true});

      const {hash} = resp[0];

      // Put the track into our database
      const track = Immutable.fromJS({
        ...tags,
        _id: `song/${hash}`,
        hash,
        createdAt: Date.now(),
      });
      yield call(db.put, track);

      // Add the track to the currently selected queue
      const currentQueue = yield this.get('selectedQueue');
      yield put(actions.addToQueue({
        trackId: track.get('_id'),
        queueId: currentQueue.get('_id'),
      }));

      yield put(actions.addTrackSuccess({hash, track}));
    },

    [actions.addToQueue]: function* ({payload: {trackId, queueId}}) {
      // Get the track and queue (to ensure they both exist)
      const queue = yield call(db.get, queueId);
      const track = yield call(db.get, trackId);

      // Append the track id to the queue
      const updatedTrackIds = queue.get('trackIds').push(track.get('_id'));
      const updatedQueue = queue.merge({trackIds: updatedTrackIds});

      yield call(db.put, updatedQueue);
      yield put(this.actions.setSelectedQueue({queue: updatedQueue}));
    },

    [actions.requeueTopTrack]: function* () {
      const selectedQueue = yield this.get('selectedQueue');
      try {
        yield call(db.put, selectedQueue);
      } catch (e) {
        console.log('Could not save updated queue');
      }
    },
  }),
});
