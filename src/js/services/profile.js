import {kea} from 'kea';
import Immutable from 'immutable';
import {delay} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import PropTypes from 'prop-types';

import db from 'utils/db';
import {callRPC} from './connection';

const ADJECTIVES = [
  'Foxy', 'Zany', 'Ominous', 'Happy', 'Leafy', 'Bouncy', 'Cheery', 'Wonderful',
  'Bald', 'Hairy', 'Smooth', 'Jazzy', 'Cold', 'Hot',
];

const ANIMALS = [
  'Gorilla', 'Penguin', 'Turtle', 'Cat', 'Dog', 'Cricket', 'Lemur', 'Lynx',
  'Turkey', 'Owl', 'Giraffe', 'Elephant',
];

const randomItem = array => array[Math.floor(Math.random() * array.length)];

const generateDefaultProfile = () => Immutable.fromJS({
  _id: 'profile',
  name: `${randomItem(ADJECTIVES)} ${randomItem(ANIMALS)}`,
  avatar: {
    topType: 'LongHairFro',
    facialHairType: 'BeardMedium',
    clotheType: 'BlazerShirt',
    skinColor: 'Brown',
    eyeType: 'Default',
    eyebrowType: 'Default',
    mouthType: 'Default',
  },
});

function* saveProfile() {
  // debounce a bit
  yield delay(1000);

  const profile = yield this.get('profile');
  const filteredProfile = profile.remove('_id').remove('_rev');
  const resp = yield callRPC({
    name: 'setProfile',
    params: {profile: filteredProfile.toJS()},
  });

  if (resp.get('error')) return;

  const peerId = resp.get('peerId');

  try {
    const {rev} = yield call(db.put, profile);
    yield put(this.actions.saveProfileSuccess({rev, peerId}));
  } catch (e) {
    console.log('Could not save profile', e);
  }
}

export default kea({
  path: () => ['kea', 'services', 'profile'],

  actions: () => ({
    init: true,
    setProfile: ({profile}) => ({profile}),
    setVariant: ({key, value}) => ({key, value}),
    saveProfileSuccess: ({rev, peerId}) => ({rev, peerId}),
    setName: ({name}) => ({name}),
  }),

  reducers: ({actions}) => ({
    profile: [null, PropTypes.instanceOf(Immutable.Map), {
      [actions.saveProfileSuccess]: (state, {rev, peerId}) => {
        return state.merge({_rev: rev, id: peerId});
      },
      [actions.setProfile]: (state, {profile}) => profile,
      [actions.setVariant]: (state, {key, value}) => state.setIn(['avatar', key], value),
      [actions.setName]: (state, {name}) => state.merge({name}),
    }],
  }),

  takeEvery: ({actions}) => ({
    [actions.init]: function* () {
      let profile;
      try {
        profile = yield call(db.get, 'profile');
      } catch (e) {
        if (e.name !== 'not_found') {
          console.log('Could not fetch profile', e);
          return;
        }

        // Create a new default profile
        profile = generateDefaultProfile();
      }

      yield put(actions.setProfile({profile}));
    },
  }),

  takeLatest: ({actions}) => ({
    [actions.setProfile]: saveProfile,
    [actions.setVariant]: saveProfile,
    [actions.setName]: saveProfile,
  }),
});
