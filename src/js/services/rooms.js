import {kea} from 'kea';
import {put} from 'redux-saga/effects';
import PropTypes from 'prop-types';
import Immutable from 'immutable';

import ProfileService from './profile';
import ConnectionService, {callRPC} from './connection';

export default kea({
  path: () => ['kea', 'services', 'rooms'],

  connect: {
    actions: [
      ConnectionService, ['send', 'receive'],
      ProfileService, ['saveProfileSuccess'],
    ],

    props: [
      ProfileService, ['profile'],
    ],
  },

  ////
  // Actions
  //
  actions: () => ({
    fetchRooms: true,
    fetchRoomsSuccess: ({rooms}) => ({rooms}),

    createRoom: ({name}) => ({name}),
    createRoomSuccess: ({room}) => ({room}),

    joinRoom: ({id}) => ({id}),
    joinRoomSuccess: ({room}) => ({room}),
    joinRoomFailure: true,

    becomeDj: true,
    setDjs: ({djs}) => ({djs}),
    setPeers: ({peers}) => ({peers}),
    setPeerProfile: ({id, profile}) => ({id, profile}),
  }),

  ////
  // Reducers
  //
  reducers: ({actions}) => ({
    rooms: [new Immutable.List(), PropTypes.instanceOf(Immutable.List), {
      [actions.fetchRoomsSuccess]: (state, {rooms}) => Immutable.fromJS(rooms),
    }],

    currentRoom: [null, PropTypes.instanceOf(Immutable.Map), {
      [actions.joinRoomSuccess]: (_, {room}) => room,
      [actions.setDjs]: (room, {djs}) => room.merge({djs}),
      [actions.setPeers]: (room, {peers}) => room.merge({peers}),
      [actions.setPeerProfile]: (room, {id, profile}) => {
        const peerIndex = room.get('peers').findIndex(p => p.get('id') === id);
        const updatedRoom = room.setIn(
          ['peers', peerIndex, 'profile'],
          Immutable.fromJS(profile),
        );
        return updatedRoom;
      },
    }],
  }),

  ////
  // Selectors
  //
  selectors: ({selectors}) => ({
    peers: [
      () => [selectors.currentRoom, ProfileService.selectors.profile],
      (room, profile) => {
        if (!room || !profile) return new Immutable.List();
        const peers = room.get('peers');
        const selfIndex = peers.findIndex(p => p.get('id') === profile.get('id'));
        return peers.setIn([selfIndex, 'profile'], profile);
      },
      PropTypes.instanceOf(Immutable.List),
    ],

    djs: [
      () => [selectors.currentRoom, selectors.peers],
      (room, peers) => peers.filter(p => room.get('djs').includes(p.get('id'))),
      PropTypes.instanceOf(Immutable.List),
    ],

    audience: [
      () => [selectors.currentRoom, selectors.peers],
      (room, peers) => peers.filter(p => !room.get('djs').includes(p.get('id'))),
      PropTypes.instanceOf(Immutable.List),
    ],
  }),

  ////
  // Sagas
  //
  takeEvery: ({actions}) => ({
    [actions.fetchRooms]: function* () {
      const rooms = yield callRPC({name: 'fetchRooms'});
      yield put(actions.fetchRoomsSuccess({rooms}));
    },

    [actions.createRoom]: function* ({payload: {name}}) {
      const room = yield callRPC({name: 'createRoom', params: {name}});
      yield put(actions.createRoomSuccess({room}));
    },

    [actions.joinRoom]: function* ({payload: {id}}) {
      const room = yield callRPC({name: 'joinRoom', params: {id}});
      if (room.get('error')) yield put(actions.joinRoomFailure());
      yield put(actions.joinRoomSuccess({room}));
    },

    [actions.becomeDj]: function* () {
      const resp = yield callRPC({name: 'becomeDj'});
      if (resp.get('error')) return;
    },

    [actions.receive]: function* ({payload: {name, params}}) {
      switch (name) {
        case 'setPeerProfile': {
          yield put(this.actions.setPeerProfile({
            id: params.id,
            profile: params.profile,
          }));
          break;
        } case 'setPeers': {
          yield put(this.actions.setPeers({peers: params.peers}));
          break
        } case 'setDjs': {
          yield put(this.actions.setDjs({djs: params.djs}));
          break;
        }
      }
    },
  }),
});
