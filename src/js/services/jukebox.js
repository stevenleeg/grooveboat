import {kea} from 'kea';
import Immutable from 'immutable';
import PropTypes from 'prop-types';
import {put, take, call} from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';

import LibraryService from './library';
import ConnectionService, {callRPC} from './connection';

const player = new Audio();
window.player = player;

const playerChannel = () => {
  return eventChannel((emitter) => {
    const handler = () => emitter({event: 'ended'});

    player.addEventListener('ended', handler);

    return () => {
      player.removeEventListener('ended', handler);
      player.pause();
    };
  });
};

export default kea({
  path: () => ['kea', 'services', 'jukebox'],

  connect: {
    actions: [
      ConnectionService, ['receive'],
      LibraryService, ['requeueTopTrack'],
    ],

    props: [
      LibraryService, ['selectedQueueWithTracks'],
    ],
  },

  actions: () => ({
    init: true,
    playTrack: ({track}) => ({track}),
    stopTrack: true,
    trackEnded: true,
  }),

  reducers: ({actions}) => ({
    currentTrack: [null, PropTypes.instanceOf(Immutable.Map), {
      [actions.playTrack]: (state, {track}) => Immutable.fromJS(track),
      [actions.stopTrack]: () => null,
      [actions.trackEnded]: () => null,
    }],
  }),

  takeEvery: ({actions}) => ({
    [actions.init]: function* () {
      const playerEvents = yield call(playerChannel);
      while (true) {
        const {event} = yield take(playerEvents);

        if (event === 'ended') {
          yield put(actions.requeueTopTrack());
          yield put(actions.trackEnded());
        }
      }
    },

    [actions.playTrack]: function* ({payload: {track}}) {
      const file = yield call(window.ipfs.cat, track.hash);
      player.src = `data:audio/mp3;base64,${file.toString('base64')}`;
      player.currentTime = 0;
      player.load();
      player.play();
    },

    [actions.stopTrack]: function () {
      player.pause();
    },

    [actions.trackEnded]: function* () {
      yield callRPC({name: 'trackEnded'});
    },

    [actions.receive]: function* ({payload: {name, params, callback}}) {
      switch (name) {
        case 'playTrack': {
          yield put(this.actions.playTrack({track: params.track}));
          break;
        } case 'stopTrack': {
          yield put(this.actions.stopTrack());
          break;
        } case 'requestTrack': {
          const currentQueue = yield this.get('selectedQueueWithTracks');
          const track = currentQueue.get('tracks').first();
          callback({track: track ? track.toJS() : null});
          break;
        }
      }
    },
  }),
});
