import {kea} from 'kea';
import {call, take, put, select} from 'redux-saga/effects';
import {delay, eventChannel, END} from 'redux-saga';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import Immutable from 'immutable';

let socket;

const openSocket = () => {
  return eventChannel((emitter) => {
    socket = io('http://localhost:8000');
    socket.on('connect', () => {
      emitter(true);
      emitter(END);
    });

    return () => {};
  });
};

const listenToSocket = () => {
  return eventChannel((emitter) => {
    socket.on('call', (payload, callback) => {
      emitter({payload, callback});
    });
    socket.on('disconnect', () => emitter(END));
    return () => socket.disconnect();
  });
};

const ConnectionService = kea({
  path: () => ['kea', 'services', 'connection'],

  actions: () => ({
    connect: true,
    connectSuccess: () => ({}),
    disconnect: true,
    send: ({name, params, callback}) => ({name, params, callback}),
    receive: ({name, params, callback}) => ({name, params, callback}),
  }),

  reducers: ({actions}) => ({
    connected: [false, PropTypes.bool, {
      [actions.connectSuccess]: () => true,
      [actions.disconnect]: () => false,
    }],
  }),

  takeEvery: ({actions}) => ({
    [actions.connect]: function* () {
      const socketChannel = yield call(openSocket);
      yield take(socketChannel);
      yield put(actions.connectSuccess());

      const messageChannel = yield call(listenToSocket);
      try {
        while (true) {
          const {payload, callback} = yield take(messageChannel);
          yield put(actions.receive({...payload, callback}));
        }
      } finally {
        console.log('Connection closed');
      }
    },
  }),
});

export function* callRPC({name, params}) {
  let connected = yield select(ConnectionService.selectors.connected);
  while (!connected) {
    yield delay(500);
    connected = yield select(ConnectionService.selectors.connected);
  }

  yield put(ConnectionService.actions.send({name, params}));
  const resp = yield call(() => new Promise((resolve) => {
    socket.emit('call', {name, params}, payload => resolve(payload));
  }));
  yield put(ConnectionService.actions.receive({name, params: resp}));

  return Immutable.fromJS(resp);
}

export default ConnectionService;
