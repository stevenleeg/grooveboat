const OFF = 0, WARN = 1, ERROR = 2;

module.exports = {
  'parser': 'babel-eslint',
  'extends': 'airbnb',
  'env': {
    browser: true
  },
  'rules': {
    'no-console': OFF,
    'default-case': OFF,
    'spaced-comment': OFF,
    'no-restricted-syntax': OFF,
    'arrow-body-style': OFF,
    'func-names': OFF,
    'object-shorthand': OFF,
    'object-curly-spacing': OFF,
    'react/jsx-filename-extension': OFF,
    'react/jsx-one-expression-per-line': OFF,
    'react/no-array-index-key': OFF,
  },
};
